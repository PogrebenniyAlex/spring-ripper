package quoter;

public interface ProfilingControllerMBean {
    public void setEnabled(Boolean enabled);
}
