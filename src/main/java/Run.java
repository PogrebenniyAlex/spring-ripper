import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import quoter.Quoter;
import quoter.TerminatorQuoter;

@Configuration
@ComponentScan("quoter")
public class Run {

    public static void main(String[] args) throws InterruptedException {
        ApplicationContext context = new AnnotationConfigApplicationContext(Run.class);
        Quoter quoter = context.getBean(Quoter.class);
        quoter.sayQuote();

    }

    @Bean
    public static Quoter quoter(){
        TerminatorQuoter quoter  = new TerminatorQuoter();
        quoter.setMessage("hi man");
        return quoter;
    }
}

